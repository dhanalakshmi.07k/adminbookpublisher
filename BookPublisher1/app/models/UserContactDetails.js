/**
 * Created by zendynamix on 06-11-2015.
 */


var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


var userContactDetails = new mongoose.Schema(
  {
    name:String,
    email :String,
    Message :String,

  },{collection:'ContactDetails'});
mongoose.model('ContactDetails', userContactDetails);
