var mongoose = require('mongoose');
var stdio = require('stdio');

/*mongoose.connect('mongodb://localhost/book_publisher');*/
mongoose.connect('mongodb://development:devPassw0rd@ds051990.mongolab.com:51990/bookish');
var bookSchema = new mongoose.Schema(
  {
    bookId : Number,
    IsbnNumber10 : Number,
    IsbnNumber13 : Number,
    Title : String,
    ShortDesc : String,
    LongDesc : String,
    PublishedDate : Date,
    Language : String,
    Category : {
      names : [
         String
      ]
    },
    PageNo: Number,
    Author: {
      names: [
        String
      ]
    },
    Format: {
      format: [
        String
      ]
    },
    thumbnail : String,
    isDeleteBook:Number,
 /*   Images: {
      thumbnail: String,
      backcover: String,
      Preview: {
        Previews: [
          String
        ]
      }
    },*/
    thumbnail : String,
    Images : [ {
      path : String,
      contentType : String,
      imageName : String
    }],
    Series: String,
    Edition: Number,
    PublisherRating: Number,
    copyRight: Number,
    PhysicalProperties: {
      dimension: {height:Number,width:Number},
      weight: Number
    },
    price: Number
  },{collection: "bookDetails1"});
/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
var Books =  mongoose.model('books',bookSchema);
/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Ends++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

var createDocument = function(noOfBooks){
  var data = noOfBooks;
  var Categories = ["Combined Graduate Level "," Combined Higher Secondary(10+2) Level","CAPF","CISF"," Delhi Police Examination"," Constable (GD)"," Stenographer Grade C, D"," Jr. Hindi Translator"," Group-D","  ASM/Goods Gard"," Asst. Loco Pilot","ESM","  PO","Clerck"];
  var BookTitle = ["Quantitative","Mathematics","English Vocabulary","Computer","Marketing","Finance"];
  var Author = ["Anand","Bharath","Ajit","Michael","Christopher","Shankara","Madhwa"];
  var Language = ["Kannada","Hindi","Tamil","Telugu","English"];
  var CopyRight = [1991,1992,2000,2001,2002,2003,2004,2005,2006,2007,2007,2008,2009,2010,2011,2012,2013,2014,2015];
  var numbers = [1,2,3];
  from = new Date(1900, 0, 1).getTime();
  to = new Date(2020, 0, 1).getTime();
  for(var i=1;i<data;i++){
    var book = new Books;
    var noOfCategories = numbers[Math.floor(Math.random()*numbers.length)];
    var noOfAuthors = numbers[Math.floor(Math.random()*numbers.length)];
    book.bookId = i;
    book.IsbnNumber10 = Math.floor(1000000000 + Math.random() * 900000000);
    book.IsbnNumber13 = Math.floor(1000000000000 + Math.random() * 900000000);
    book.Title= BookTitle[Math.floor(Math.random()*BookTitle.length)]+i;
    book.isDeleteBook=1;
    book.ShortDesc = "This is a goood book ,you will defnitely love it";
    book.LongDesc = "This is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love itThis is a goood book ,you will defnitely love it";
    book.PublishedDate = new Date(from + Math.random() * (to - from));
    book.Language = Language[Math.floor(Math.random()*Language.length)];
    for(var j=0;j<=2;j++){
      book.Category.names[j]= Categories[Math.floor(Math.random()*Categories.length)];
    }
    for(var k=0;k<=2;k++){
      book.Author.names[k] = Author[Math.floor(Math.random()*Author.length)];
    }
    book.PageNo = Math.floor(Math.random()*202);
   /* book.thumbnail = "../img/img"+i+".jpg";*/
    book.Format.format = "MixedMedia";
    book.Format.format = "PaperBack";
   /* book.Images.thumbnail = "img/thm/Book"+i+".jpg";*/
    var imageDetails = {
      imageName: "Book"+i+".jpg"
    }
    book.Images.push(imageDetails);
    /*book.Images.backcover = "img/bkCover/Book"+i+".jpg";
    for(var k=0;k<=3;k++){
      book.Images.Preview.Previews[k] = "img/prevew/Book1_"+k+".jpg";
    }*/
    book.Series = "First Edition";
    book.Edition = 1;
    book.PublisherRating = Math.floor(Math.random()*11);
    book.copyRight = CopyRight[Math.floor(Math.random()*CopyRight.length)];
    book.PhysicalProperties.dimension.height = 12.9;
    book.PhysicalProperties.dimension.width = 19.8;
    book.PhysicalProperties.weight = 5;
    book.price = Math.floor(Math.random()*1011);
    book.save(function(err,books){
      if(err)
        console.log(err);

    });
  }
};
stdio.question('Enter Number Of Book Documents To Be Created', function (err, noOfBooks) {
  createDocument(noOfBooks);
  console.log(noOfBooks);
});
