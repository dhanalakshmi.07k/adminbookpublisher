/**
 * Created by Suhas on 12/11/15.
 */

$(document).ready(function () {

  var parentEle=$("#wrapper");
  fun={
    dropDown: function () {
      parentEle.on("click",".dropDown .head",function (e) {
        e.stopPropagation();
        var init= $(this).data("init");
        if(init=="0"){
          $(this).parent().find(".list").slideDown(200);
          $(this).data({
            "init":"1"
          })
        }
        else{
          $(this).parent().find(".list").slideUp(200);
          $(this).data({
            "init":"0"
          })
        }
      });
      parentEle.on("click",".dropDown .list li",function (e) {
        var value=$(this).text();
        $(this).parents().eq(1).find(".headTitle").text(value);
      })
    },
    defaultClick: function(){
      $(document).click(function () {
        //$(".notifications").removeClass("bounceInRight").addClass("bounceOutRight");
        //$("#header .notificationAlert").data({
        //    'init':'0'
        //}).removeClass("active");
        //setTimeout(function () {
        //    $(".notifications").fadeOut(0)
        //},800);


        $(".dropDown .list").slideUp(200);
        $(".mapMenu").slideUp(200);
        $(".dropDown .head").data({
          "init":"0"
        });

        $(".notificationList .actionBtn").slideUp(200);
        $(".notificationList li").data({
          'init':'0'
        });

        $(".popup").css({
          'visibility': 'hidden'
        });
        setTimeout(function () {
          $(".background").fadeOut(300);
        },200);

        $(".loadingLanes .num .associatedImg").fadeOut(0);

      })
    },
    preventDefaultClicks: function () {
      var selectors='.notificationList, .notificationList .actionBtn, .popup, .loadingLanes .num';

      parentEle.on("click",selectors,function (e) {
        e.stopImmediatePropagation()
      })
    }
  };
  fun.dropDown();
  fun.defaultClick();
  fun.preventDefaultClicks();
  $('.search-icon-books').click(function(){
    $('nav .mobile-view').toggle('fast');
    $('nav .mobile-view').css('width','100%');
    $('nav .navbar-custom-menu').toggle();
    $('.search-icon-books').toggle('fast')
  })
  $('.search-mobile-view').click(function(){
    $('nav div a.search-mobile-view').toggle('fast');
    $('nav div.mobile-view-searchWidth').toggle('fast');
    $('nav div.navbar-custom-menu').toggle('fast');
  })
  $('.mobile-view-button').click(function(){
    $('nav div a.search-mobile-view').toggle('fast');
    $('nav div.mobile-view-searchWidth').toggle('fast');
    $('nav div.navbar-custom-menu').toggle('fast');
  })
  /*$.('.highlightAll').click(function(){

   })*/
  /*$('.sidebar-click').click(function() {
    $('aside ul span a.sidebar-click').css('backgroundColor','yellow');
  });*/
  $('.subCategoryHighlight').click(function(){
    $('aside ul li a.to-be-change .subCategoryHighlight').css('color','yellow');
    /*$('aside ul ul li a.to-be-changed').css('backgroundColor','yellow');*/
  })
});
