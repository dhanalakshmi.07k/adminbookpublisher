var bookPublisherApp = angular.module('bookPublisherApp', ['ngRoute','imageSlider','ngSanitize','ui.bootstrap']);/*,'ngAnimate'*/


bookPublisherApp.service('MetaService', function() {
  var title = 'Lucent Publication';
  var metaDescription = '';
  var metaKeywords = '';
  return {
    set: function(newTitle, newMetaDescription, newKeywords) {
      metaKeywords = newKeywords;
      metaDescription = newMetaDescription;
      title = newTitle;
    },
    metaTitle: function(){ return title; },
    metaDescription: function() { return metaDescription; },
    metaKeywords: function() { return metaKeywords; }
  }
});

bookPublisherApp.run(function($rootScope,$http) {
  $rootScope.imageBaseUrl = null;
    $http.get("imageBaseUrl").then(function(response){
      $rootScope.imageBaseUrl = response.data;
      console.log("imageBaseUrl="+$rootScope.imageBaseUrl);
    })
});


/* 'angular-google-analytics' bookPublisherApp.config(function(AnalyticsProvider) {
  // initial configuration
  AnalyticsProvider.setAccount('UA-75228772-1');
  // track all routes/states (or not)
  AnalyticsProvider.trackPages(true);
  // Use analytics.js instead of ga.js
  AnalyticsProvider.useAnalytics(true);
  AnalyticsProvider.setDomainName('none');

})*/
/*bookPublisherApp.run(function(Analytics) {
    // In case you are relying on automatic page tracking, you need to inject Analytics
    // at least once in your application (for example in the main run() block)
  })*/

bookPublisherApp.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
bookPublisherApp.controller('bookCtrlr', function ($scope,bookService,$http,$parse,$modal,typeaheadSrchService,$routeParams) {
  /*$scope.ImageContainerLL =["img/prevew/Book1_0.jpg","img/prevew/Book1_1.jpg"];*//*,"img/prevew/Book1_2.jpg"*/


  /*function generateMetaDataController() {
    bookService.getMainCategoryForMetaDetails().then(function(result) {
      var categoryMetaData=[];
      for(var k=0;k<result.data.length;k++){
        categoryMetaData.push(result.data[k].categoryName)
      }
      $rootScope.metaservice = MetaService;
      $rootScope.metaservice.set("Web App","desc",categoryMetaData.toString())
    });

  }
  generateMetaDataController()*/


  var limit = 10;
  var totalNoOfBooks;
  var length;
  var master={};
  $scope.showmeLess = true;
  $scope.showmeMore = false;
  $scope.noOfBooksPerPage=12;
  $scope.noOfBooksPerPageArray = [10,20,30];
  $scope.linkState = [];
  var ImagePreview = [];
  var currentPage = 1;
  var paginationCriteria = "all";
  var authorSelected;
  var categorySelected;
  var noOfBooksPerPage = 10;
  $scope.noBooks = true;
  var type = "normal";
  var IncrementButton = false;
  $scope.leftCara = true;
  $scope.rightCara = true;
  $scope.searchIndex='';
  $scope.loaderStatus=false;
  $scope.all = "all";
  $scope.clickedElm = -1;
/*  $scope.metadata = {
    'title': 'default title'
  };

  $scope.$on('onLoadIndexPage', function(event, categoryMetadata) {
    console.log("***********************************")
    console.log(categoryMetadata)
    console.log("***********************************")
    $scope.metadata = categoryMetadata;

  });


  function generateMetaDataController() {
    bookService.getMainCategoryForMetaDetails().then(function(result) {
      var categoryMetaData=[];
      for(var k=0;k<result.data.length;k++){
        categoryMetaData.push(result.data[k].categoryName)
      }
      $scope.$emit('onLoadIndexPage', { 'title': categoryMetaData.toString() });
    });
    // the event gets fired when the page is loaded and the controller is called.
  }
  generateMetaDataController()*/;

  bookService.getAllAuthors().then(function(result){
    $scope.authorDetails= result.data;
  });
  bookService.getMainCategories().then(function(result){
    $scope.mainCategories= result.data;
  });
  $scope.getBookByRange = function(pageNo){
    $scope.loaderStatus=true;
    var booksPerPage = $scope.noOfBooksPerPage;
    var start = ((pageNo-1)*booksPerPage);
    $scope.startingBook =start;
    currentPage = pageNo;
    $scope.currentPageNo = pageNo;
    var end = booksPerPage;
    $scope.firstBookInThisrange =start;
    $scope.lastBookInThisrange = start +(booksPerPage *1);
    if(paginationCriteria === "all"){
      $scope.setBreadCrumb("home");
      var end = booksPerPage;
      bookService.getBooksByrangeForPage(start,end).then(function(result){
        $scope.bookDetails = result.data;
        $scope.changeRoute('#!/');
        $scope.loaderStatus=false;
        if($scope.bookDetails.length == 0){
          $scope.noBooks = false;
        }else{
          $scope.noBooks = true;
        }

      });
    }else if(paginationCriteria === "category"){
      bookService.getBooksByCategoryNameAndRangeForPage(categorySelected,start,end).then(function(result){
        $scope.bookDetails = result.data;
        $scope.loaderStatus=false;
        if($scope.bookDetails.length == 0){
          $scope.noBooks = false;
        }else{
          $scope.noBooks = true;
        }
      });
    }else if(paginationCriteria === "author"){
      bookService.getBooksByAuthorNameAndRangeForPage(authorSelected,start,end).then(function(result){
        $scope.bookDetails = result.data;
        $scope.loaderStatus=false;
        if($scope.bookDetails.length == 0){
          $scope.noBooks = false;
        }else{
          $scope.noBooks = true;
        }
      });
    }else if(paginationCriteria === "searchIndex"){
      bookService.getBookDetailsBySearchCriteria($scope.searchIndex,start,$scope.noOfBooksPerPage).then(function(result) {
        $scope.bookDetails = result.data;
        $scope.loaderStatus=false;
        if($scope.bookDetails.length == 0){
          $scope.noBooks = false;
        }else{
          $scope.noBooks = true;
        }
      });
    }
    else if(paginationCriteria === "title"){
      /*bookService.getBookDetailsBySearchCriteria($scope.searchIndex,start,$scope.noOfBooksPerPage).then(function(result) {*/
      var bookByTitle =[];
        bookByTitle.push( $scope.singleBookDetails);
        $scope.bookDetails = bookByTitle;
        $scope.loaderStatus=false;
        if($scope.bookDetails.length == 0){
          $scope.noBooks = false;
        }else{
          $scope.noBooks = true;
        }
      /*});*/
    }
  };
  $scope.setPagination = function(type,pageNo){
    length = Math.round(totalNoOfBooks/$scope.noOfBooksPerPage);
    $scope.totalNoOfPages = length;
    var totalLength =  $scope.totalNoOfPages;
    var lengthArray = [];
    var startingPage;
    if(length<1){
      $scope.pageArraylength = 1;
    }else if(length>5 && type == "normal"){
      $scope.pageArraylength = 5;
      $scope.startingPage=0;
    }else if(length<5 && type == "normal"){
      $scope.pageArraylength = length;
      $scope.startingPage=0;
    }
    if(type == "normal"){
      pageNo = 1;
      $scope.startingPage = 0;
    }else if(type == "Increment" && $scope.startingPage+5<length){
      $scope.startingPage =  $scope.startingPage+1;
      $scope.pageArraylength = $scope.pageArraylength+1;
    }else if(type == "Decrement" && $scope.startingPage+5<length){
      $scope.startingPage =  $scope.startingPage-1;
      $scope.pageArraylength = $scope.pageArraylength-1;
    }else if(type == "Skip" && pageNo && pageNo+2<=length){
      if(pageNo<=length){
        if(pageNo-3<0){
          $scope.startingPage =  0;
          $scope.pageArraylength = $scope.startingPage+5;
        }else{
          $scope.startingPage =  pageNo-3;
          $scope.pageArraylength = $scope.startingPage+5;
        }
      }
    }else if(type == "Skip" && pageNo && pageNo+2>=length){
      $scope.pageArraylength = length;
    }
    if($scope.startingPage+5 >= length){
      IncrementButton = true;
    }
    for(var i=$scope.startingPage;i<$scope.pageArraylength;i++) {
      lengthArray.push(i);
    }
    $scope.lengthArray = lengthArray;
    if(pageNo && pageNo<=length+1){
      $scope.getBookByRange(pageNo);
    }else if(pageNo>length+1){
      $scope.currentPageNo=1;
      /*$scope.getBookByRange($scope.currentPageNo);*/
    }else{
      $scope.currentPageNo=1;
      $scope.getBookByRange($scope.currentPageNo);
    }
  };
  $scope.getTotalBooksCount = function(data) {
    //paginationCriteria = data;
    var  pageNo = 1
    if(paginationCriteria === "all"){
      bookService.getTotalBooksCount().then(function (result) {
        totalNoOfBooks = result.data.NoOfBooks;
        type = "normal";
        $scope.setPagination(type,pageNo);
        $scope.totalBooks = totalNoOfBooks;
      });
    }else if(paginationCriteria === "category"){
      bookService.getTotalBooksCountByCategory(data).then(function (result) {
        totalNoOfBooks = result.data.NoOfBooks;
        type = "normal";
        $scope.setPagination(type,pageNo);
        $scope.totalBooks = totalNoOfBooks;
      });
    }else if(paginationCriteria === "author"){
      bookService.getTotalBooksCountByAuthor(data).then(function (result) {
        totalNoOfBooks = result.data.NoOfBooks;
        type = "normal";
        $scope.setPagination(type,pageNo);
        $scope.totalBooks = totalNoOfBooks;
      });

    }else if(paginationCriteria === "searchIndex"){
      bookService.getCountOfSearchedBookDetails(data).then(function(result) {
        totalNoOfBooks = result.data.NoOfBooks;
        type = "normal";
       $scope.setPagination(type,pageNo);
        $scope.totalBooks = totalNoOfBooks;
      })
    }
    else if(paginationCriteria === "title"){
     /* bookService.getCountOfSearchedBookDetails(data).then(function(result) {*/
        totalNoOfBooks =data;
        type = "normal";
      $scope.setPagination(type,pageNo);
        $scope.totalBooks = totalNoOfBooks;
     /* })*/
    }
  };

 if($routeParams.MainCategoryName && $routeParams.subCategoryName){
   $scope.getBookByCategory($routeParams.MainCategoryName,$routeParams.subCategoryName)
 }
  else{

   $scope.breadCrumb = {
     main : "Home",
     mainBranch:['All Books'],
     secondary:"Home",
     secondaryBranch:['All Books']
   };
   init();
 }
  $scope.getBookByCategory = function(mainCategory,subCategory,index){
    currentPage = 1;
    paginationCriteria = "category";

    if(!subCategory){
      var category = mainCategory;
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = mainCategory;
      $scope.breadCrumb.secondaryBranch.push(mainCategory);
    }
    else{
      var category = subCategory;
      $scope.mainCategory = mainCategory;
      $scope.subCategory = subCategory;
        $scope.setBreadCrumb("category")
    }


    categorySelected = category;
    $scope.getTotalBooksCount(category);
  };
  $scope.getBookByAuthor = function(data){
    currentPage = 1;
    paginationCriteria = "author";
    var author = data;
    authorSelected = data;
    $scope.authName = data;
    $scope.setBreadCrumb("author");
    $scope.getTotalBooksCount(author)
  };
  $scope.getBookByName=function(bookDetails){
    paginationCriteria = "title"
    $scope.setBreadCrumb("title");
    $scope.getTotalBooksCount(bookDetails)
  }
  $scope.getAllBooksByAll = function(){
    $routeParams.mainCategoryName = " ";
    $routeParams.subCategoryName = " ";
    currentPage = 1;
    $scope.currentPageNo=1;
    paginationCriteria = "all";
    $scope.getTotalBooksCount(paginationCriteria)
  };
  $scope.getMoreBooksByLimit = function(){
    var pageNo = currentPage+1;
    if(pageNo<=$scope.totalNoOfPages){
      type = "Increment";
      $scope.setPagination(type,pageNo);
    }
  };
  $scope.getLessBooksByLimit = function(){
    var pageNo = currentPage-1;
    if(pageNo!=0){
      type = "Decrement";
      $scope.setPagination(type,pageNo);
    }
  };
  $scope.getNewBooks = function(bookNo){
    $scope.selectedIndex = -2;
    bookService.getNewBooks().then(function(result){
      $scope.bookDetails = result.data;
      if($scope.bookDetails.length == 0){
        $scope.noBooks = false;
      }else{
        $scope.noBooks = true;
      }
    });
  };
  $scope.getBookDetailsByBookId = function(data){
      var bookId = data;
      var catArray;
      bookService.getBookByBookId(bookId).then(function(result){
        $scope.singleBookDetails = result.data;
        bookDetails = $scope.singleBookDetails;
        $scope.ImageContainerLL = [];
        for(var i = 0;i<$scope.singleBookDetails.Images.length;i++){
            var imageNAme = $scope.imageBaseUrl+$scope.singleBookDetails.Images[i].imageName;
          $scope.ImageContainerLL.push(imageNAme);
        }
        console.log($scope.ImageContainerLL);
        catArray =  $scope.singleBookDetails.Category.names;
        $scope.getRelatedBooks(catArray);
        $scope.bookDetailsModal(bookDetails);
      });

  };
  $scope.setBooksPerPage = function(booksPerPage){
    type = "normal";
    $scope.noOfBooksPerPage = booksPerPage;
    pageNo = 1;
    $scope.setPagination(type,pageNo);
  };
  $scope.changeRoute = function (url, forceReload) {
        $scope = $scope || angular.element(document).scope();
        if (forceReload || $scope.$$phase) { // that's right TWO dollar signs: $$phase
          window.location = url;
        } else {
          $location.path(url);
          $scope.$apply();
        }
      };
  $scope.addUserQueries = function(userDetails){
    // var queryDetails = data;
    console.log("inside add query")
    if (userDetails != undefined && userDetails.name!= undefined &&userDetails.email!= undefined&&userDetails.message!= undefined){
      $scope.master = angular.copy(userDetails);
      $http.post('/ContactDetails',$scope.master
      ).success(function (data, status, headers, config) {
          console.log("Success!");
         // sendmail(userDetails);
        /*  $scope.sucessMessage="Thanks for the query we will contact you soon"*/
      $scope.changeRoute('#/messageSentSuccess');          /*    alert("User added Successfully");*/
        }).error(function (data, status, headers, config) {
          console.log("Error");
        });
    }

    /*bookService.userQueries($scope.master).then(function(result){
     console.log("Success!")
     alert("User query posted Successfully");
     });*/
  };
  $scope.getRelatedBooks = function(cateArray){
    var array = cateArray[0];
    bookService.getAllRelatedBooks(array).then(function(result){
      $scope.relatedBookArray= result.data;
      document.getElementById('second')
    });
  }
  $scope.setBreadCrumb = function(type){
    if(type == "aboutUs"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = "Home";
      $scope.breadCrumb.mainBranch.push("About Us");
      $scope.breadCrumb.secondaryBranch.push("About Us");
    }else if(type == "contactUs"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = "Home";
      $scope.breadCrumb.mainBranch.push("ContactUs");
      $scope.breadCrumb.secondaryBranch.push("ContactUs");
    }else if(type == "home"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = "Home";
      $scope.breadCrumb.mainBranch.push("All Books");
      $scope.breadCrumb.secondary= "Home" ;
      $scope.breadCrumb.secondaryBranch.push("All Books");
    }
    else if(type == "MainCategoryBooks"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = "Home";
      $scope.breadCrumb.mainBranch.push( $scope.mainCatName);
      $scope.breadCrumb.secondary= "Home" ;
      $scope.breadCrumb.secondaryBranch.push( $scope.mainCatName);
    }
    else if(type == "category"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
     /* $scope.breadCrumb.thirdMainBranch = [];*/
     /* $scope.breadCrumb.thirdSecondaryBranch = [];*/
      $scope.breadCrumb.main = $scope.mainCategory;
      $scope.breadCrumb.mainBranch.push($scope.subCategory);
     /* $scope.breadCrumb.thirdMainBranch.push($scope.subCategory)*/
      $scope.breadCrumb.secondary= $scope.mainCategory ;
      $scope.breadCrumb.secondaryBranch.push($scope.subCategory);
     /* $scope.breadCrumb.thirdSecondaryBranch.push($scope.subCategory);*/
    }
    else if(type == "author"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = ''
      $scope.breadCrumb.mainBranch.push("Books Related To Author"+" "+">"+" "+$scope.authName);
      $scope.breadCrumb.secondary= "Home" ;
      $scope.breadCrumb.secondaryBranch.push("Search Result");
    }
    else if(type == "title"){
      $scope.breadCrumb.mainBranch = [];
      $scope.breadCrumb.secondaryBranch = [];
      $scope.breadCrumb.main = ''
      $scope.breadCrumb.mainBranch.push("Search Result For Book"+" "+">"+" "+$scope.bookTitle);
      $scope.breadCrumb.secondary= "Home" ;
      $scope.breadCrumb.secondaryBranch.push("Search Result");
    }
  }
  $scope.bookDetailsModal = function (bookDetail) {
    console.log("BookDetails");
    $scope.singleBookDetails=bookDetail;
    var modalInstance = $modal.open({
      scope: $scope,
      templateUrl: 'view/modal/bookDetail.html',
      controller: 'bookDetailsById',
      size: 'lg',
      animation: true,
    });
    $scope.selectedBook = "";
  }
  $scope.locationURL="/search";
  $scope.searchBook = function(searchName) {
    $scope.searchIndex=searchName;
    paginationCriteria = "searchIndex";
    $scope.breadCrumb.mainBranch = [];
    $scope.breadCrumb.secondaryBranch = [];
    $scope.breadCrumb.main = "Search";
    $scope.breadCrumb.mainBranch.push(searchName);
    $scope.breadCrumb.secondaryBranch.push(searchName);
    $scope.getTotalBooksCount($scope.searchIndex)
  };

  $scope.getImagePathOfs3=function(){
    $scope.imagePathOfBook=bookService.getS3ImagePath();
  }
  $scope.getImagePathOfs3();
  /*$scope.bookByTitleForTypeAhead = function(){
    bookService.bookByBookTitle().then(function(res){
        console.log(res);
      $scope.typeAheadArray = res.data;
      console.log($scope.typeAheadArray);
    })
  }*/
  /*$scope.searchFunction = {
    booksByTitle:function(searchParameter){
    if(searchParameter){
      typeaheadSrchService.getBooksByTitle(searchParameter).then(function(res){
        var typeAheadArray = [];
        if(res && res.data && res.data.length){
          for(var i =0;i<res.data.length;i++){
            var title = res.data[i].Title;
            console.log(title);
            var typeObj = {
              value:title,
              type:"Title"
            }
            typeAheadArray.push(typeObj);
            if(typeAheadArray.length === res.data.length ){
              $scope.typeAheadArray=typeAheadArray;
            }
          }
        }
      });
    }
  },
    booksByCategory:function(searchByCategoryName){
    if(searchByCategoryName){
      typeaheadSrchService.getBooksByCategoryName(searchByCategoryName).then(function(res){
        var typeAheadArray  = [];
        if(res && res.data && res.data.length>0){
          for(var i=0;i<res.data.length;i++){
            var subCategoryObj = res.data[i];
            var typeaheadObj = {
              value:subCategoryObj._id,
              type:"subCategory"
            }
            typeAheadArray.push(typeaheadObj)
            if(typeAheadArray.length == res.data.length){
              $scope.typeAheadArray  = typeAheadArray;
            }
          }
        }
      });
    }
  },
    booksByAuthor:function(searchByAuthName){
      if(searchByAuthName){
        typeaheadSrchService.getBooksByAuthName(searchByAuthName).then(function(res){
          var typeAheadeData  = res.data;
          var typeAheadArray = [];
          if(res && res.data && res.data.length){
            for(var i =0;i<res.data.length;i++){
              var data= res.data[i].nickName;
              console.log(data)
              var typeAheadObj = {
                value:data,
                type:"Author"
              }
              typeAheadArray.push(typeAheadObj);
              if(typeAheadArray.length === res.data.length ){
                $scope.typeAheadArray  = typeAheadArray;
              }
            }
            console.log( $scope.typeAheadArray)
          }
        });
      }
    }
  }*/
  $scope.searchBookByFilter = function(searchParameter){
    $scope.typeAheadArray=[];
    var searchString = "getBooksBy"+$scope.filterBY;
    /*$scope.searchFunction["booksBy"+$scope.filterBY](selectedBook);*/
    if(searchParameter){
      typeaheadSrchService[searchString](searchParameter).then(function(res){
        var typeAheadeData  = res.data;
        /*console.log(res.data)*/
        var typeAheadArray = [];
        if(res && res.data && res.data.length){
          $scope.typeAheadArray  = res.data;
          /*for(var i =0;i<res.data.length;i++){
           var data= res.data[i].value;
           console.log(data)
           var typeAheadObj = {
           value:data
           }
           typeAheadArray.push(typeAheadObj);
           if(typeAheadArray.length === res.data.length ){
           $scope.typeAheadArray  = typeAheadArray;
           }
           }*/
        }
      });
    }
  }

$scope.clearInput = function(type){

if(type === "Title"){
  $scope.selectedBook = "" ;
}
  else if(type === "Category"){
  $scope.selectedBook = "";
}
else if(type === "Author"){
  $scope.selectedBook = "";
}
}
//TypeAhead On Select

  $scope.getBookDetailsBasedOnFilter = function(searchParameter){
    console.log(searchParameter);
     if($scope.filterBY === "Title"){
       var title = searchParameter.value;
      /* console.log(title);*/
       typeaheadSrchService.getBookTitleDetails(title).then(function(result){
        /* console.log(result.data)*/
         $scope.singleBookDetails = result.data;
         $scope.bookTitle = result.data.Title
         /*console.log($scope.singleBookDetails)*/
         bookDetails = $scope.singleBookDetails;
         $scope.ImageContainerLL = [];
         for(var i = 0;i<$scope.singleBookDetails.Images.length;i++){
           var imageNAme = $scope.imageBaseUrl+$scope.singleBookDetails.Images[i].imageName;
           $scope.ImageContainerLL.push(imageNAme);
         }
         console.log($scope.ImageContainerLL);
         catArray =  $scope.singleBookDetails.Category.names;
         $scope.getRelatedBooks(catArray);
        /* $scope.bookDetailsModal(bookDetails);*/
         $scope.getBookByName(bookDetails);
       })
     }
    else if($scope.filterBY === "Author"){
       var authName = searchParameter.value;
       /*typeaheadSrchService.getBookAuhtorDetails(authName).then(function(result){
         console.log(result.data);
       })*/
       $scope.getBookByAuthor(authName);
       $scope.selectedBook = "";
     }
    else if($scope.filterBY === "Category"){
       var catName = searchParameter.value;
       $scope.getBookByCategory(catName);
       $scope.selectedBook = "";
     }
  }

  if ($routeParams.mainCategoryName) {
    $scope.BooksByMainCategory($routeParams.mainCategoryName);
  }
  $scope.BooksByMainCategory =function(mainCatName){
    $scope.mainCatName = mainCatName;
    bookService.getBooksByMainCategory(mainCatName).then(function(result){
      $scope.setBreadCrumb("MainCategoryBooks");
      console.log(result.data);
      $scope.bookCatDetails = result.data;

    })
  }
  function init(){
   /* $scope.getImagePathOfs3();*/
    $scope.getTotalBooksCount(paginationCriteria);
    /*$scope.bookByTitleForTypeAhead();*/
  }

  $("body").on("click",".subCategoryHighlight",function () {
    $(".subCategoryHighlight").removeClass("active");
    $(this).addClass("active");
  });



});


bookPublisherApp.directive('autoCompleteDirective',function($http){
  function encodingFunction(uri) {
    var res = encodeURIComponent(uri);
    return res;
  }
  var books = [];
  /* var encode=encodingFunction(request.term);*/
  return{
    restrict:'A',
    scope:{
      url:'@'
    },
    link:function(scope,elm,attrs){
      elm.autocomplete({
        source:function(request,response){
          $http({method:'get',url:scope.url,params:{search:encodingFunction(request.term)}}).success(function(data){
            scope.name=data;
            console.log(data);
            books = [];
            for(var k=0;k<data.length;k++){
              console.log(data[k]. Title)
              books.push(data[k]. Title)
            }
            response(books);
          })
        },
        minLength:4
      })
    }
  }
})



