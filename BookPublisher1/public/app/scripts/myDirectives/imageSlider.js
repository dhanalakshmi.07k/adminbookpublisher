var imageSlider = angular.module('imageSlider', []);
imageSlider.directive('imageSlider', function($timeout) {
  return {
    restrict: 'E',
    scope: {
      arrayImage: '@arrayImage'
    },
    link: function(scope, elem, attrs) {
      console.log(attrs.imageArray);
        if(attrs.imageArray){
          var ImageContainer =JSON.parse(attrs.imageArray);
        }else{
          var ImageContainer =["img/prevew/Book1_0.jpg","img/prevew/Book1_1.jpg","img/prevew/Book1_2.jpg"];
        }
        scope.incrementImagesForSlider= function(){
          scope.leftCara = false;
          if(scope.currentImageIndex<scope.imagePreviewLength-1){
            scope.currentImageIndex= scope.currentImageIndex+1;
            scope.image = ImagePreview[scope.currentImageIndex];
          }else{
            scopes.rightCara = false;
          }
        };
        scope.decrementImagesForSlider = function(){
          scope.rightCara = true;
          if(scope.currentImageIndex>0){
            scope.currentImageIndex= scope.currentImageIndex-1;
            scope.image = ImagePreview[scope.currentImageIndex];
          }else{
            scope.leftCara = true;
          }
        };
        ImagePreview = [];
        scope.imagePreview = [];
        for( var i=0;i<ImageContainer.length;i++){
          ImagePreviewname =ImageContainer[i];
          ImagePreview.push(ImagePreviewname);
        }
        scope.imagePreview = ImagePreview;
        scope.imagePreviewLength = ImagePreview.length;
        scope.currentImageIndex = 0;
        scope.image = ImagePreview[scope.currentImageIndex];
        if(ImagePreview.length === 1){
          scope.rightCara = false;
          scope.leftCara = true;
        }else{
          scope.rightCara = true;
        }
    },
    templateUrl: 'view/slider.html'
  };

});
