bookPublisherApp.directive('toggleClass', function() {
  var lastActiveElement=null;
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind('click', function() {
        if(!lastActiveElement){
          lastActiveElement = element;
        }else{
          lastActiveElement.toggleClass(attrs.toggleClass);
          lastActiveElement = element;
        }
        element.toggleClass(attrs.toggleClass);
      });
    }
  };
});
/*

// we create a simple directive to modify behavior of <ul>
bookPublisherApp.directive("whenScrolled", function(){
  return{

    restrict: 'A',
    link: function(scope, elem, attrs){

      // we get a list of elements of size 1 and need the first element
      raw = elem[0];

      // we load more elements when scrolled past a limit
      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;

          // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});*/
/*$scope.getClass = function(ind){
 /!*if( ind === $scope.selectedIndex){
 return "cbp-filter-item-active";
 } else{
 return "";
 }*!/
 };*/
/*$scope.getMoreBooksByLimit = function(bookNo){
 var limit = bookNo.length+limit;
 if(limit<totalNoOfBooks){
 bookService.getAllBooksByLimit(limit).then(function(result){
 $scope.bookDetails = result.data;
 });
 }

 };
 $scope.getLessBooksByLimit = function(bookNo){
 var limit = bookNo.length-limit;
 if(limit<totalNoOfBooks){
 bookService.getAllBooksByLimit(limit).then(function(result){
 $scope.bookDetails = result.data;
 });
 }
 };*/
