/**
 * Created by sandeep on 7/6/2016.
 */
bookPublisherApp.factory('typeaheadSrchService', function($q,$http,$window){
  var bookDetails;
  var getBooksByTitle = function(searchBookTitle){
    if(searchBookTitle){
      return $http.get("/book/regex/title/"+searchBookTitle)
    }
  }

  var getBooksByCategory = function(searchBookcat){
    if(searchBookcat){
      return $http.get("/book/regex/Category/"+searchBookcat)
    }
  }

  var getBooksByAuthor = function(searchBookAuth){
    if(searchBookAuth){
      return $http.get("/book/regex/Author/"+searchBookAuth)
    }
  }
  var getBookTitleDetails =function(bookTitle){
    return $http.get("/book/by/title/"+bookTitle)
  }
  return{
     getBooksByTitle:getBooksByTitle,
    getBooksByCategory:getBooksByCategory,
    getBooksByAuthor:getBooksByAuthor,
    getBookTitleDetails:getBookTitleDetails
  }
})
