'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var reloadPort = 35730, files;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    production: {
      server: {
        file: 'Distribution/app.js'
      }
    },
    develop: {
      server: {
        file: 'app.js'
      }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      js: {
        files: [
          'app.js',
          'app/**/*.js',
          'config/*.js'
        ],
        tasks: ['develop', 'delayed-livereload']
      },
      css: {
        files: [
          'public/css/*.css'
        ],
        options: {
          livereload: reloadPort
        }
      },
      views: {
        files: [
          'app/views/*.jade',
          'app/views/**/*.jade'
        ],
        options: { livereload: reloadPort }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          '../BookPublisherDistribution/public/app/libaries/minifiedScripts/appScript-min.js':['public/app/cdnLinks/angular.min.js',
          'public/app/scripts/controller/controllers.js',
          'public/app/scripts/controller/bookDetailsctrl.js',
          'public/app/scripts/controller/categoryctrl.js',
          'public/app/cdnLinks/route.js',
          'public/app/cdnLinks/angularJsBootstrap/ui-bootstrap-tpls-0.13.1.js',
          'public/app/scripts/myDirectives/bookPublisherDirectives.js',
          'public/app/scripts/route/bookPublisherRoute.js',
          'public/app/scripts/service/bookPublisherService.js',
          'public/app/scripts/myDirectives/imageSlider.js',
          'public/app/cdnLinks/bootstrap/js/bootstrap.min.js',
          'public/app/scripts/myDirectives/slider.js',
          'public/app/testCss/plugins/slimScroll/jquery.slimscroll.min.js',
            'public/app/scripts/script.js'
         ],
         }
      },
      target: {
        options:{mangle: true},
         files: {
          '../BookPublisherDistribution/public/app/libaries/minifiedScripts/app-min.js':'public/app/testCss/dist/js/app.js'
         }
        }
    },
    processhtml: {
      build: {
        files: {
          '../BookPublisherDistribution/public/app/index.html': ['public/app/index.html']
        }
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, cwd: 'app/', src: ['**'], dest: '../BookPublisherDistribution/app/'},
          {expand: true, cwd: 'config/', src: ['**'], dest: '../BookPublisherDistribution/config/'},
          {expand: true, cwd: 'public/app', src: ['**','!**/scripts/**','!**/styles/**','!index.html'], dest: '../BookPublisherDistribution/public/app'},
          { src: "package.json", dest: '../BookPublisherDistribution/'},
          { src:"app.js", dest: '../BookPublisherDistribution/'},
          {expand: true, cwd: 'public/app/testCss', src: ['**'], dest: '../BookPublisherDistribution/public/app/testCss/plugins'}
        ]
      }
    },
    cssmin: {
      target: {
        files: {
          '../BookPublisherDistribution/public/app/libaries/cssMinified/template-min.css': ['public/app/cdnLinks/bootstrap/css/bootstrap.min.css',
          'public/app/testCss/dist/css/AdminLTE.css',
          'public/app/testCss/dist/css/skins/skin-yellow-light.min.css',
          'public/app/styles/bookPublisherCSS.css']
        }
      }
    },
    htmlSnapshot: {
      debug: {
        options: {
          snapshotPath: 'snapshots/',
          sitePath: 'http://localhost:7800/#!/',
          msWaitForPages: 1000,
          //sanitize function to be used for filenames. Converts '#!/' to '_' as default
          //has a filename argument, must have a return that is a sanitized string
          /*sanitize: function (requestUri) {
            //returns 'index.html' if the url is '/', otherwise a prefix
            if (/\/$/.test(requestUri)) {
              return 'index.html';
            } else {
              return requestUri.replace(/\//g, 'prefix-');
            }
          },*/
          urls: [
            '',
            '/aboutUs',
            '/contactUs',
            '/category/UPSC Exams/Civil Services Exam (CSE)',
            '/category/IBPS Exams/IBPS - SO',
            '/category/SSC Exams/Combined Graduate Level (CGL) Exam Tire-1',
            '/category/BPSC & BSSC Exams/Bihar Public Service Commission PT & Mains',
            '/category/Railway Exams/Commercial Apprentice (CA)'
          ]
        }
      }
      /*prod: {
        options: {}
      } to run this grunt task use:grunt htmlSnapshot*/
    }
  });

  grunt.config.requires('watch.js.files');
  files = grunt.config('watch.js.files');
  files = grunt.file.expand(files);
  grunt.loadNpmTasks('grunt-contrib-uglify'); // load the given tasks
  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-html-snapshot');


  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function(err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded)
            grunt.log.ok('Delayed live reload successful.');
          else
            grunt.log.error('Unable to make a delayed live reload.');
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('default', ['develop','watch']);
  grunt.registerTask('production', ['uglify','copy','processhtml','cssmin']);
};
