var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.use(compress());
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';

  app.set('views', config.root + '/app/views');
  app.set('view engine', 'jade');

  // app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root+'/public/app'));
  app.use(express.static(config.root+'../../../bookish',{maxAge:"356d"}));

  app.use(methodOverride());
  console.log("-------------Directoryname---------------");
  console.log(config.root);
  /*app.use(express.static(__dirname+'/public/app'));*/



  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });
  /*To deliver static HTML using NodeJS and Express (the web application framework for NodeJS)*/
  app.use(function(req, res, next) {
    var fragment = req.query._escaped_fragment_;

    // If there is no fragment in the query params
    // then we're not serving a crawler
    if (!fragment) return next();

    // If the fragment is empty, serve the
    // index page
    if (fragment === "" || fragment === "/")
      fragment = "/index.html";

    // If fragment does not start with '/'
    // prepend it to our fragment
    if (fragment.charAt(0) !== "/")
      fragment = '/' + fragment;

    // If fragment does not end with '.html'
    // append it to the fragment
    if (fragment.indexOf('.html') == -1)
      fragment += ".html";

    // Serve the static html snapshot
    try {
      var file = __dirname + "/snapshots" + fragment;
      res.sendfile(file);
    } catch (err) {
      res.send(404);
    }
  });

};
