var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var bookSchemaDef = new mongoose.Schema(
  {
    bookId : Number,
    sortId:Number,
    IsbnNumber10 : Number,
    IsbnNumber13 : Number,
    Title : String ,
    ShortDesc : String,
    LongDesc : String,
    PublishedDate : Date ,
    Language : String,
    isPublished:Number,
    PrintingPrice:Number,
    isDeleteBook:Number,
    Category : {
      names : [
        String
      ]
    },
    PageNo : Number,
    Author: {
      names: [
        String
      ]
    },
    Format: {
      format: [
        String
      ]
    },
    Images : [ {
      path : String,
      contentType : String,
      imageName : String
    }],

    Series :String,
    Edition : Number,
    PublisherRating : Number,
    copyRight : String,
    PhysicalProperties: {
      dimension: {
        height:String,
        width:String
      },
      weight: String
    },
    price : Number,
    ecartLink:String,
    createdAt:{type:Date,default:Date.now},
    updateAt:{type:Date,default:Date.now}
  },{collection: "Books"});
bookSchemaDef.index({ Title: 'text'});
bookSchemaDef.pre('update',function(){
  this.update({},{ $set: { updateAt: new Date() } });
})
/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('Books',bookSchemaDef);
