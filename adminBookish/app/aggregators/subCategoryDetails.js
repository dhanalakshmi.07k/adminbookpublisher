/**
 * Created by zendynamix on 16-02-2016.
 */

    var mongoose = require('mongoose'),
    Categories= mongoose.model('Categories');

var showSubCategoryDetails=function(page,pageSize,res){
    /*console.log("inside servciee"+page);
    console.log("inside servciee"+pageSize);*/
    Categories.aggregate(
        [
            {$unwind: "$subCategory"},
            {$project: {_id:0,categoryName:1,categoryId:1,subCategory:1}},
            {$sort: {"subCategory.createdOn":-1}},
            { $skip : parseInt(page) },
            { $limit : parseInt(pageSize) }
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}


var showSubTestCategoryDetails=function(res){

    Categories.aggregate(
        [
            {$unwind: "$subCategory"},
            {$project: {_id:0,categoryName:1,categoryId:1,subCategory:1}},
            {$sort: {"subCategory.createdOn":-1}}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}

var subCategoryDropDown=function(res){
    Categories.aggregate(
        [
            {$unwind: "$subCategory"},
            {$project: {_id:0,subCategory:1}},
            {$sort: {"subCategory.createdOn":-1}}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}






var addSubcategory = function(req,res){
    Categories.subCategory=[];

    Categories.aggregate(
        [
            { $match : { categoryName : req.body.categoryName} },
            {$project: {"numberOfSubCategories": { $size: "$subCategory" }}}
        ],
        function (err, result) {
            Categories.findOne({ categoryName: req.body.categoryName}, function (err, catRes){
                var subCategory = {
                    name : req.body.subCategory.name,
                    description:req.body.subCategory.categoryDescription,
                    categoryName:req.body.subCategory.categoryName,
                    sortId:result[0].numberOfSubCategories+1
                };
                catRes.subCategory.push(subCategory);
                catRes.save();
                res.send("successfuly added SubCategory")
            });
        })


}

var subCategoryCount=function(res){
    Categories.aggregate(
        [
            {$unwind: "$subCategory"},
            { $group: { _id: null, count: { $sum: 1 }}}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })

}


var showSubCategoryBasedOnCategory=function(searchParameter,res,page,pageSize){
    Categories.aggregate(
        [
            {$match : { categoryName : searchParameter } } ,
            {$unwind: "$subCategory"},
            {$project: {_id:0,categoryName:1,categoryId:1,subCategory:1}},
            {$sort: {"subCategory.sortId":1}},
            { $skip : parseInt(page) },
            { $limit : parseInt(pageSize) }
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}

var showAllSubCategoryBasedOnCategory=function(searchParameter,res){
    Categories.aggregate(
        [
            {$match : { categoryName : searchParameter } } ,
            {$unwind: "$subCategory"},
            {$project: {_id:0,categoryName:1,categoryId:1,subCategory:1}},
            {$sort: {"subCategory.sortId":1}}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}


module.exports={
    getCategoryData:showSubCategoryDetails,
    getSubCategoryCount:subCategoryCount,
    addSubcategory:addSubcategory,
    subCategoryDropDown:subCategoryDropDown,
    showSubTestCategoryDetails:showSubTestCategoryDetails,
    getSubcategoryByCategory:showSubCategoryBasedOnCategory,
    getAllSubCategories:showAllSubCategoryBasedOnCategory
}