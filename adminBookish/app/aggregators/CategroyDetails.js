/**
 * Created by zendynamix on 26-02-2016.
 */

var mongoose = require('mongoose'),
    Categories= mongoose.model('Categories');

var showMainCategoryDetails=function(page,pageSize,res){

    Categories.aggregate(
        [
            {$project: {_id:0,categoryName:1,categoryId:1,categoryDescription:1}},
            {$sort: {"categoryId":-1}},
            { $skip : parseInt(page) },
            { $limit : parseInt(pageSize)}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}

var mainCategoryCount=function(res){
    Categories.aggregate(
        [

            { $group: { _id: "result", count: { $sum: 1 }}}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })

}

var mainSubCategoryCount=function(res,categoryName){
    Categories.aggregate(
        [
            { $match : { categoryName : categoryName} },
            {$project: {"numberOfSubCategories": { $size: "$subCategory" }}}
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        })
}


module.exports={
    getMainCategoryData:showMainCategoryDetails,
    mainCountCategory:mainCategoryCount,
    mainSubCategoryCount:mainSubCategoryCount
}