/**
 * Created by zendynamix on 10-10-2015.
 */

/**
 * Created by zendynamix on 29-09-2015.
 */
var express = require('express');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var mongoose = require('mongoose');
var router = express.Router();
var config = require('../../config/config.js');
var morgan      = require('morgan');
var cors = require('cors');
module.exports = function (app) {
  app.use(router);
};

router.use(morgan('development'));
UserOuth =  mongoose.model('UserOauth');
var superSecret=config.secret;
router.get('/setup', function(req, res) {

  // create a sample user
  var nick = new UserOuth({
    name: 'Nick Cerminara',
    password: 'password',
    admin: true
  });

  // save the sample user
  nick.save(function(err) {
    if (err) throw err;

    console.log('User saved successfully');
    res.json({ success: true });
  });
});



/*router.get('/test', function(req, res) {
  res.json({ message: 'Welcome to the coolest API on earth!' });
});*/





// route to authenticate a user (POST http://localhost:8080/api/authenticate)
router.post('/authenticate', function(req, res) {

  // find the user
  UserOuth.findOne({
    name: req.body.name
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {

      // check if password matches
      if (user.password != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {

        // if user is found and password is right
        // create a token
        var token = jwt.sign(user.name,'zendynamix', {
          expiresInMinutes: 1440 // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      }

    }

  });
});



// route middleware to verify a token
/*router.use(function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, 'zendynamix', function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
});*/


// route to return all users (GET http://localhost:8080/api/users)
router.get('/users', function(req, res) {
  UserOuth.find({}, function(err, users) {
    res.json(users);
  });
});






