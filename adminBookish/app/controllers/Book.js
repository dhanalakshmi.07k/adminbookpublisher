
var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var logger = require('../../config/logger.js');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
var uploadBookImagesTos3 = require('../s3Upload');
var config=require('../../config/config')




module.exports = function (app) {
  app.use(router);
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
};

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Books =  mongoose.model('Books');
Categories= mongoose.model('Categories');
Authors= mongoose.model('Authors');

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Stops++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});


/*+++++++++++++++++++++++++++++++++++++++++   Router  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
router.route('/api/book/:id')
  .get(function(req,res){
    Books.find({bookId : req.params.id},function(err,books){
      res.send(books);
    })
  });


router.route('/all/images/ById/:bookId')
    .get(function(req,res){
      Books.find({bookId : req.params.bookId},{"_id":0,"Images":1},function(err,images){
        res.send(images);
      })
    });


router.route('/api/delete/book/:id')
  .post(function(req,res){
    Books.findOne({bookId: req.params.id}, function(err,updateRemoveBook) {
      updateRemoveBook.isDeleteBook= 0;
      updateRemoveBook.save();
      res.send(updateRemoveBook);
    });
  });


router.route('/api/unarchive/Book/:id')
  .post(function(req,res){
    Books.findOne({bookId: req.params.id}, function(err,updateRemoveBook) {
      updateRemoveBook.isDeleteBook= 1;
      updateRemoveBook.save();
      res.send(updateRemoveBook);
    });
  });


router.route('/api/Book/Delete/Image')
  .put(function(req,res){
    Books.findOne({ bookId: req.body.bookId}, function (err, BookDelRes){
      BookDelRes.Images=req.body.Images;
      BookDelRes.save();
      res.send("book deleted upadted ")
    });
  });



router.route('/api/publish/book/:bookPublishId')
  .post(function(req,res){
    Books.findOne({ bookId: req.params.bookPublishId}, function (err, bookres){
      bookres.isPublished=1;
      bookres.save();
      res.send("published")
    });
  });


router.route('/api/unPublish/book/:bookunPublishId')
  .post(function(req,res){
    Books.findOne({ bookId: req.params.bookunPublishId}, function (err, bookres){
      bookres.isPublished=0;
      bookres.save();
      res.send("unpublished")
    });
  });


router.route('/api/addBook')
  .post(function(req,res){
      console.log("inside add book");
      console.log(req.body.sortId)
    var books = new Books();
    books.bookId= req.body.bookId;
    books.sortId= req.body.sortId;
    books.IsbnNumber10 = req.body.IsbnNumber10;
    books.IsbnNumber13= req.body.IsbnNumber13;
    books.Title= req.body.Title;
    books.ShortDesc = req.body.ShortDesc;
    books.LongDesc = req.body.LongDesc;
    books.isDeleteBook=1;
    books.isPublished=0;
    books.PrintingPrice=req.body.PrintingPrice;
    books.PublishedDate = req.body.PublishedDate;
    books.Language= req.body.Language;


    if(req.body.Category!=undefined) {
      books.Category.names = req.body.Category.names;
    }
    books.PageNo  = req.body.PageNo;
    if(req.body.Author!=undefined){
      books.Author.names= req.body.Author.names;
    }
    if(req.body.Format!=undefined) {
      books.Format.format = req.body.Format.format;
    }
    books.Series = req.body.Series;
    books.Edition = req.body.Edition;
    /*books.PublisherRating = req.body.PublisherRating;*/
    books.copyRight  = req.body.copyRight;
    books.price= req.body.price;
    if(req.body.PhysicalProperties!=undefined){
      books.PhysicalProperties.dimension.height=req.body.PhysicalProperties.dimension.height;
    }
    if(req.body.PhysicalProperties!=undefined){
      books.PhysicalProperties.dimension.width=req.body.PhysicalProperties.dimension.width;
    }
    if(req.body.PhysicalProperties!=undefined){
      books.PhysicalProperties.weight=req.body.PhysicalProperties.weight;
    }
      books.ecartLink=req.body.ecartLink;
    books.save(function(err,result){
      if(err)
        res.send(err);
      res.send(result);
    })
  });



router.route('/api/updateBookbyId')
  .post(function(req,res){
    Books.findOne({ bookId: req.body.bookId}, function (err, bookres){
      /*bookres.bookId= req.body.bookId;*/
      if(req.body.IsbnNumber10!=undefined){
        bookres.IsbnNumber10 = req.body.IsbnNumber10;
      }
      if(req.body.IsbnNumber13!=undefined){
        bookres.IsbnNumber13= req.body.IsbnNumber13;
      }
      if(req.body.Title!=undefined){
        bookres.Title= req.body.Title;
      }
      if(req.body.ShortDesc!=undefined){
        bookres.ShortDesc = req.body.ShortDesc;
      }

      if(req.body.LongDesc!=undefined){
        bookres.LongDesc = req.body.LongDesc;
      }

      if(req.body.PublishedDate!=undefined){
        bookres.PublishedDate = req.body.PublishedDate;

      }
      if(req.body.PrintingPrice!=undefined){
        bookres.PrintingPrice = req.body.PrintingPrice;

      }
      if(req.body.Language!=undefined){
        bookres.Language= req.body.Language;
      }
      if(req.body.Category!=undefined){
        bookres.Category= req.body.Category;
      }

      if(req.body.PageNo!=undefined){
        bookres.PageNo  = req.body.PageNo;
      }

      if(req.body.Author!=undefined){
        bookres.Author= req.body.Author;
      }

      if(req.body.Format!=undefined){
        bookres.Format = req.body.Format;
      }

      if(req.body.Series!=undefined){
        bookres.Series = req.body.Series;
      }
      if(req.body.Edition!=undefined){
        bookres.Edition = req.body.Edition;
      }

      if(req.body.PublisherRating!=undefined){
        bookres.PublisherRating = req.body.PublisherRating;
      }

      if(req.body.copyRight!=undefined){
        bookres.copyRight  = req.body.copyRight;
      }
      if(req.body.Edition!=undefined){
        bookres.Edition = req.body.Edition;
      }

      if(req.body.PhysicalProperties!=undefined){
        bookres.PhysicalProperties.dimension.height=req.body.PhysicalProperties.dimension.height;
      }

      if(req.body.PhysicalProperties!=undefined){
        bookres.PhysicalProperties.dimension.width=req.body.PhysicalProperties.dimension.width;
      }

      if(req.body.PhysicalProperties!=undefined){
        bookres.PhysicalProperties.weight=req.body.PhysicalProperties.weight;
      }

      if(req.body.price!=undefined){
        bookres.price= req.body.price;
      }
      bookres.save();
      res.send(bookres)
    });
  });



router.route('/book/image/:updateImageByBookId')
    .post(function(req,res){
      Books.findOne({ bookId:req.params.updateImageByBookId}, function (err, bookRes){
        console.log("***************"+req.params.updateImageByBookId);
        console.log(req.files);
        var tempfile  = req.files;
        var fileDetails=req.files.files;
        var filePath=req.files.files.path;
        var imageData  = {
          path  :tempfile.files.path,
          contentType :'image/jpg',
          imageName :tempfile.files.name
        };
        bookRes.Images.push(imageData);
        bookRes.save();
        uploadBookImagesTos3.uploadImage.saveImageToAwsS3(fileDetails,filePath);
        res.send("image  updated to book")

      });

    })

router.route('/api/books/:start/:end')
  .get(function(req,res){
    Books.find(function(err,books){
      if(err)
        res.send(err);
      res.send(books);
    }).skip(req.params.start).limit(req.params.end).sort({ bookId: -1 });

  });

router.route('/book/update/bySort/')
  .post(function(req,res){
      var sortedBookList=req.body.sortedBookArray;
      for(var k=0;k<sortedBookList.length;k++){
        var originalBookId=sortedBookList[k].bookId;
        var sortedBookId=sortedBookList[k].sortId;
        updateSortedID(originalBookId,sortedBookId,res);
      }
      res.send("updated");
  });


function  updateSortedID(originalBookId,sortedBookId){
  Books.findOne({ bookId: originalBookId}, function (err, bookres){
    bookres.sortId=sortedBookId;
    bookres.save();

  });

}

router.route('/books/publish')
    .get(function(req,res){
      Books.find(function(err,books){
        if(err)
          res.send(err);
        res.send(books);
      }).sort({ updateAt: -1 });
    });

router.route('/api/books/count')
  .get(function(req,res){
    Books.count(function(err,books){
      if(err)
        res.send(err);
      var count = {NoOfBooks: books};
      res.send(count);
    });
  });


router.route('/api/findAllBooks')
  .get(function(req,res){
    Books.find(function(err,books){
      if(err){
        res.send(err);
        logger.info("the default logger with my tricked out transports is rockin this module");
      }

      res.send(books);

    });
  });


router.route('/api/deleteAllBook')
  .get(function(req,res){
    // delete the book
    Books.remove(function(err) {
      if (err)
        res.send(err);
      res.send("book deleted")

    });
  });







router.route('/search')
    .get(function(req,res){
      Books.find(
          { Title: {$regex:req.query.search, $options:'i'} },{"_id":0,"Title":1},function(err,books){
        res.send(books);
      }).limit(5);
    })


router.route('/search/book/name/:searchName')
    .get(function(req,res) {
      var searchParameter = "\"" + req.params.searchName + "\"";
      Books.find({$text: {$search: searchParameter}}, function (err, books) {
        res.send(books);
      })
    })


router.get('/push/subCategory/snapShot/Book/:oldCategoryName/:newCategoryName', function (req, res) {

  var oldSubCategory= req.params.oldCategoryName;
  var newSubCategory= req.params.newCategoryName;

  Books.update(
      {"Category.names":oldSubCategory},
      { $push: { "Category.names" : newSubCategory} },
      { multi: true }, function (err, catRes){
        res.send("successfuly pushed SubCategory")
      })
})




router.get('/push/authour/snapShot/Book/:oldNickName/:newNickName', function (req, res) {
  var oldNickName= req.params.oldNickName;
  var newNickName= req.params.newNickName;
  console.log(oldNickName);
  console.log(newNickName);
  Books.update(
      {"Author.names":oldNickName},
      { $push: { "Author.names" : newNickName} },
      { multi: true }, function (err, response){


      })
      res.send("pushed")
})



router.get('/pull/authour/snapShot/Book/:oldNickName', function (req, res) {
  console.log("inside pull");
  var oldNickName= req.params.oldNickName;
  Books.update(
      {"Author.names":oldNickName},
      {$pull: { "Author.names" : oldNickName} },
      { multi: true }, function (err, response){

      })
      res.send("successfuly authour pulled")
})


router.get('/sort/all/books/', function (req, res) {

  Books.find(
      {},
      { bookId: 1,Title:1 },function (err, bookRes) {
        if (err) {
          res.send(err)
        }
        res.send(bookRes)
      }).sort({sortId:1})
})

router.get('/all/published/books/:start/:end', function (req, res) {
 Books.find(
 {isPublished:1},
 {  },function (err, bookRes) {
 if (err) {
 res.send(err)
 }
 res.send(bookRes)
 }).skip(req.params.start).limit(req.params.end).sort({ createdAt: -1 });
 })

router.get('/all/Unpublished/books/:start/:end', function (req, res) {

  Books.find(
      {isPublished:0},
      {  },function (err, bookRes) {
        if (err) {
          res.send(err)
        }
        res.send(bookRes)
      }).skip(req.params.start).limit(req.params.end).sort({ createdAt: -1 });
})


router.get('/all/unPublished/books/', function (req, res) {

  Books.find(
      {isPublished:0},
      {  },function (err, bookRes) {
        if (err) {
          res.send(err)
        }
        res.send(bookRes)
      }).sort({bookId:1})
})


router.route('/publish/books/count')
    .get(function(req,res){
      Books.count({isPublished:1},function(err,books){
        if(err)
          res.send(err);
        var count = {NoOfPublishedBooks: books};
        res.send(count);
      });
    });


router.route('/unPublish/books/count')
    .get(function(req,res){
      Books.count({isPublished:0},function(err,books){
        if(err)
          res.send(err);
        var count = {NoOfUnPublishedBooks: books};
        res.send(count);
      });
    });

/*+++++++++++++++++++++++++++++++++++++++++  Additional service calls ends++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
router.get('/pull/subCategory/snapShot/Book/:oldCategoryName', function (req, res) {console.log("inside push");
  var oldSubCategory= req.params.oldCategoryName;
  Books.update(
      {"Category.names":oldSubCategory},
      {$pull: { "Category.names" : oldSubCategory} },
      { multi: true }, function (err, catRes){
        res.send("successfuly pulled SubCategory")
      })
})


router.get('/delete/subCategory/snapShot/Book/:subCategoryName', function (req, res) {
  console.log("inside pull");
  var subCategoryName= req.params.subCategoryName;
  Books.update(
      {"Category.names":subCategoryName},
      {$pull: { "Category.names" : subCategoryName} },
      { multi: true }, function (err, catRes){
        res.send("successfuly pulled SubCategory")
      })
})

router.route('/api/all/category')
    .get(function(req,res){
      Categories.find({},'categoryName',function(err,categories){
        res.send(categories);

      }.sort({createdOn:-1}));
    });

router.route('/api/Categories')
    .get(function(req,res){
      Categories.find(function(err,books){
        if(err)
          res.send(err);
        res.send(books);
      }).sort({createdOn:-1}).limit(10);
    });




router.get('/delete/image/id/:imageName/:bookId', function (req, res) {
  console.log("inside pull");

  Books.update(
      {"bookId":req.params.bookId},
      { $pull: { Images: { _id: req.params.imageName} } },
      { multi: true }, function (err, catRes){
        uploadBookImagesTos3.uploadImage.deleteImageToAwsS3(req.params.imageName,res);
        res.send("successfuly pulled SubCategory")
      })
})


router.route('/imageBaseURL')
    .get(function(req,res){
      res.send(config.imageBaseUrl);
    })