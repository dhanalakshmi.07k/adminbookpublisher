var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt');
var secret = 'this is the secrete password';
var mainCategoryAggregate = require('../aggregators');
var categoryAggregate = require('../aggregators');

module.exports = function (app) {
  app.use(router);
};
Categories= mongoose.model('Categories');

router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized Token Is not present');
  }
});



router.route('/api/CategoryName/:categoryName')
    .get(function(req,res){

      Categories.find({categoryName:req.params.categoryName},function(err,books){
        if(err){
          res.status(500).send(err.message);
          console.log(err.stack);
        }
        res.send(books);
      });
    });



router.route('/api/CategoryUpdate/:CategoryId')
    .get(function(req,res){
      Categories.find({categoryId  : req.params.CategoryId},function(err,books){

        if(err)
          res.send(err);
        res.send(books);
        console.log("provide category by bookId");
      })
    });


router.route('/api/SubCategories/:categoryName')
    .get(function(req,res){
      Categories.find({categoryName:req.params.categoryName},function(err,categories){
        if(err)
          res.send(err);
        res.send(categories);
        console.log("Get all the subcategories");
      })
    });

router.route('/api/sort/SubCategories/:categoryName')
    .get(function(req,res){
        Categories.find({categoryName:req.params.categoryName},function(err,categories){
            if(err)
                res.send(err);
            res.send(categories);
            console.log("Get all the subcategories");
        }).sort({'subCategory.sortId': 1});
    });




router.route('/api/category/count')
    .get(function(req,res){
      Categories.count(function(err,categories){
        if(err){
          res.status(500).send(err.message);
          console.log(err.stack);
        }
        var count = {NoOfCategories: categories};
        res.send(count);
      });
    });



router.route('/api/Category')
    .post(function(req,res){

      var category = new Categories();
      category.categoryName = req.body.categoryName;
      category.categoryId = req.body.categoryId;
      category.categoryDescription= req.body.categoryDescription;
      category.isMainCategory=1;
      category.save(function(err,result){
        if(err){
          res.status(500).send(err.message);
          console.log(err.stack);
        }
        res.send(result);

      })
    });

router.route('/api/Categories')
    .get(function(req,res){
      Categories.find(function(err,books){
        if(err){
          res.status(500).send(err.message);
          console.log(err.stack);
        }
        res.send(books);
      }).sort({createdOn:-1}).limit(10);
    });



router.route('/api/Category/id')
    .post(function(req,res){

      Categories.findOne({categoryId:req.body.categoryId }, function (err, categoryRes){
        categoryRes.categoryName= req.body.categoryName;
        categoryRes.SubCategories = req.body.SubCategories;
        categoryRes.categoryDescription= req.body.categoryDescription;
        categoryRes.save();
        res.send(categoryRes)
      });
    });

router.route('/api/all/subCategory')
    .get(function(req,res){
      Categories.find(function(err,author){
        res.send(author);

      }).sort({createdOn:-1});
    });





router.route('/api/deleteAllCategories')
    .get(function(req,res){
      // delete the book
      Categories.remove(function(err) {
        if (err)
          res.send(err);
        res.send("book deleted")
        console.log('Categories deleted successfully');
      });
    });

router.route('/api/delete/Category/:id')
    .get(function(req,res){
        Categories.remove({categoryId : req.params.id}, function(err,removed) {
            if (err)
                res.send(err);
            res.send("Category deleted by id")
        });
    });


router.get('/category/pagination/:startLimit/:endLimit', function (req, res) {
    var page= req.params.startLimit;
    var pageSize= req.params.endLimit;
    mainCategoryAggregate.mainCategroyDetails.getMainCategoryData(page,pageSize,res);
})

router.get('/category/count', function (req, res) {
    mainCategoryAggregate.mainCategroyDetails.mainCountCategory(res);
})



router.get('delete/category/count/:categoryName', function (req, res) {
    var categoryName=req.params.categoryName;
    mainCategoryAggregate.mainCategroyDetails.mainSubCategoryCount(res,categoryName);
})



/*router.get('/count/:categoryName', function (req, res) {
    var categoryName=req.params.categoryName;

    mainCategoryAggregate.mainCategroyDetails.mainSubCategoryCount(res,categoryName);
})*/
/*router.route('/update/category/name')
 .post(function(req,res){
 var oldCategoryName=req.body.oldCategoryName;
 var categoryName=req.body.category;
 /!*  var subCategoryName=req.body.subCategory;*!/
 var categoryDescription=req.body.categoryDescription;
 Categories.update(
 {"categoryName" : oldCategoryName},
 { $set: { "categoryName" : categoryName,"categoryDescription" : categoryDescription} }, function (err, catRes){
 res.send("successfuly upadted SubCategory")
 })
 })






 */
router.get('/main/Categories', function (req, res) {
    Categories.find(
        { },{categoryName:1},
        function (err, catRes){
            res.send(catRes)
        }).sort({categoryId:-1})
})


router.route('/subCategory/update/bySortId/')
    .post(function(req,res){
        var sortedSubcategoryArray=req.body.sortedSubcategoryArray;
        for(var k=0;k<sortedSubcategoryArray.length;k++){
            var maincategory=sortedSubcategoryArray[k].mainCategoryName;
            var subCategoryObjectId=sortedSubcategoryArray[k].objectId;
            var subCategorySortId=sortedSubcategoryArray[k].subCategorySortId;
            updateSubcategorySortedID(maincategory,subCategoryObjectId,subCategorySortId);

        }
        res.send("updated");
    });


function  updateSubcategorySortedID(maincategory,subCategoryObjectId,subCategorySortId){

    Categories.update(
        {"categoryName" :maincategory, "subCategory._id": subCategoryObjectId},
        { $set: {"subCategory.$.sortId":subCategorySortId} },function (err, res){

        }
    )
}


router.route('/search/categoryLabel')
    .get(function(req,res){
        Categories.find({ $text : { $search : req.query.search }},{"_id":0,"categoryName":1},function(err,categories){
            res.send(categories);
        }).limit(5);
    })


router.route('/search/pagination/categoryLabel/categoryName/:searchCategoryName/:page/:pageSize')
    .get(function(req,res) {
        var page= req.params.page;
        var pageSize= req.params.pageSize;
        var searchParameter = req.params.searchCategoryName;

        categoryAggregate.subCategoryDetails.getSubcategoryByCategory(searchParameter,res,page,pageSize);
       /* Categories.find({$text: {$search: searchParameter}}, function (err, categories) {
            res.send(categories);
        })*/
    })
router.route('/search/categoryLabel/cetgoryName/:searchCategoryName')
    .get(function(req,res) {
        var page= req.params.page;
        var pageSize= req.params.pageSize;
        var searchParameter = req.params.searchCategoryName;
        categoryAggregate.subCategoryDetails.getAllSubCategories(searchParameter,res);
        /* Categories.find({$text: {$search: searchParameter}}, function (err, categories) {
         res.send(categories);
         })*/
    })

router.get('/unique/Categories/subCategory/:CategoryName/:subCategoryName', function (req, res) {
    console.log("@@@@@@@@@@");
    console.log(req.params.CategoryName)
    console.log(req.params.subCategoryName)
    Categories.find(
        {$and:[{"categoryName" : req.params.CategoryName}, {"subCategory.name" : req.params.subCategoryName }] },{categoryName:1},
        function (err, catRes){
            res.send(catRes)
        })
})


