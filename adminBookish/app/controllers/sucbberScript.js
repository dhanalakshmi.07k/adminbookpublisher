
var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var logger = require('../../config/logger.js');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';


module.exports = function (app) {
    app.use(router);
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
};

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Books =  mongoose.model('Books');
Categories= mongoose.model('Categories');
Authors= mongoose.model('Authors');

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Stops++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized Token Is not present');
    }
});

/*
router.get('/update/Subcategory/test', function (req, res) {
    var bookCategoryArray=[];
    Books.find( {}, { Category: 1,bookId: 1}, function (err, catRes){
        var i;
        for(var i=0;i<catRes.length;i++){
            for(var j=0;j<catRes[i].Category.names.length;j++){
                var bookCategoryObject={};
                bookCategoryObject.bookId=catRes[i].bookId;
                bookCategoryObject.categoryName=catRes[i].Category.names[j];
                bookCategoryArray.push(bookCategoryObject);
            }
        }
        if(i==catRes.length){
            getCategoryArray(bookCategoryArray);
        }

    })
})
function getCategoryArray(bookCategoryArray){
    var subCategoryArray=[];
    Categories.aggregate(
        [
            {$unwind: "$subCategory"},
            {$project: {_id:0,subCategory:1}}

        ],
        function (err, subCategoryObjectArray) {
            if (err) {
                console.log(err);
                return;
            }
            for(var k=0;k<subCategoryObjectArray.length;k++){
                subCategoryArray.push(subCategoryObjectArray[k].subCategory.name)
            }

            if(k==subCategoryObjectArray.length){
                updateBookNewCategories(bookCategoryArray,subCategoryArray);
            }


        })
}
function  updateBookNewCategories(bookCategoryArray,subCategoryArray){
    var resulantArray=[];
    var g;
    for(g=0;g<bookCategoryArray.length;g++){
                for(var m=0;m<subCategoryArray.length;m++){
                    if((bookCategoryArray[g].categoryName.indexOf(subCategoryArray[m])) > -1){
                      var result= bookCategoryArray[g].categoryName.replace(bookCategoryArray[g].categoryName, subCategoryArray[m]);
                        var resulantobj={};
                        resulantobj.catName=result;
                        resulantobj.bookId=bookCategoryArray[g].bookId;
                        resulantArray.push(resulantobj);
                    }
                }

    }
    if(g==bookCategoryArray.length){
        updateBooks(resulantArray);
    }

}
function  updateBooks(resulantArray){
    Books.update(
        {},{$unset:{"Category.names":1}},
        { multi: true }, function (err, bookres){
            updateBooksToDb(resulantArray);
        })

}
function updateBooksToDb(resulantArray){
    console.log("Result Arrray Length = "+resulantArray.length);
    for(var iterator=0; iterator<resulantArray.length; iterator++){
        console.log("iterator="+iterator);
       console.log(resulantArray[iterator].bookId)
        console.log(resulantArray[iterator].catName);

        Books.update(
            { "bookId" : resulantArray[0].bookId},
            { $push:{"Category.names":resulantArray[0].catName}}, function (err, res){
                console.log("success")
            }
        )

    }

}*/
router.get('/update/Subcategory/test', function (req, res) {
        var subCategoryArray=[];
        Categories.aggregate(
            [
                {$unwind: "$subCategory"},
                {$project: {_id:0,subCategory:1}}

            ],
            function (err, subCategoryObjectArray) {
                if (err) {
                    console.log(err);
                    return;
                }
                for(var k=0;k<subCategoryObjectArray.length;k++){
                    subCategoryArray.push(subCategoryObjectArray[k].subCategory.name)
                }

                if(k==subCategoryObjectArray.length){
                   matchCategory(subCategoryArray);
                }


            })
    })

function matchCategory(subCategoryArray) {
     var bookCategoryArray=[];
     Books.find( {}, function (err, booksArray){
     for(var i=0;i<booksArray.length;i++){
     findBookById(booksArray[i].bookId,subCategoryArray);
     }
     })
}

function findBookById(bookId,subCategoryArray){
    var modifiedCategoryArray=[];
    Books.find( {"bookId":bookId}, function (err, bookDetails){
         modifiedCategoryArray=bookDetails[0].Category.names;
            for(var j=0;j<bookDetails[0].Category.names.length;j++){
                for(var k=0;k<subCategoryArray.length;k++) {
                        var categoryString=bookDetails[0].Category.names[j];
                        if((categoryString.trim().indexOf("Books for"))===0){
                            var spiltCategoryString =categoryString.split("Books for");
                                if(spiltCategoryString[1].trim()===subCategoryArray[k]){
                                    var index = modifiedCategoryArray.indexOf(bookDetails[0].Category.names[j]);
                                    if (index !== -1) {
                                        modifiedCategoryArray[index]=spiltCategoryString[1].trim();
                                        break;
                                    }
                                   // modifiedCategoryArray.push(spiltCategoryString[1]);
                                }
                        }
                }
        }
        updateBookCategoryDatabase(bookId,modifiedCategoryArray);
        console.log(modifiedCategoryArray);

    })

}

function updateBookCategoryDatabase(bookId,modifiedCategoryArray){
    console.log(modifiedCategoryArray);
    Books.findOne({ bookId: bookId}, function (err, bookres){
        bookres.Category.names=modifiedCategoryArray;
        bookres.save();
        console.log("SDFASDFAd");
    });

}