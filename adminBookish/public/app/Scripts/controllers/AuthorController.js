/**
 * Created by zendynamix on 07-04-2016.
 */
app.controller('AuthorController', function ($scope, $modal, authorService, authInterceptor,$http) {

    $scope.itemsperpage = 10;
    $scope.bigCurrentPageAutour = 1;
    $scope.AuthourlocationURL="/searchAuthor";
    $scope.init = function () {
        $scope.getAuthorForPagination(1);
    }
    $scope.$on('authorAdded', function (event, args) {
        var curPage=args.currentPage;
        $scope.getAuthorForPagination(curPage);
    });
    $scope.getTotalCountOfAuthor = function () {
        authorService.getNumberOfAuthors().then(function (response) {
            var authorCount=response.data.NoOfAuthors;
            $scope.bigTotalItemsAuthors = authorCount;
            authorService.setTotalAuthours(authorCount);
        });
    }
    $scope.clearSearchResult = function (pageNo) {
        $scope.searchParameter=null;
        authorService.setAuthorPageNo(pageNo);
        var pageSelected = 1;
        if (pageNo) {
            pageSelected = pageNo;
        }
        var startPage = (pageSelected - 1) * $scope.itemsperpage;
        authorService.getAuthorsForPagination(startPage, $scope.itemsperpage).then(function (response) {
            $scope.authorDetails = response.data;
            $scope.getTotalCountOfAuthor();

        });
    }

    $scope.getAuthorForPagination = function (pageNo) {
        authorService.setAuthorPageNo(pageNo);
        var pageSelected = 1;
        if (pageNo) {
            pageSelected = pageNo;
        }
        var startPage = (pageSelected - 1) * $scope.itemsperpage;
        authorService.getAuthorsForPagination(startPage, $scope.itemsperpage).then(function (response) {
            $scope.authorDetails = response.data;
            $scope.getTotalCountOfAuthor();

        });
    }
    $scope.openDeleteAuthor = function (author) {
        $scope.authorIdDetails = author;
        var modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/DeleteAuthor.html',
            controller: 'AuthorDialogController',
            size: 'lg',
            animation: true,
        });
    }
    $scope.searchAuthorBook = function(searchName) {
        $http.get("/search/Author/name/"+searchName).then(function(response) {
            $scope.authorDetails = response.data;
            $scope.bigTotalItemsAuthors=response.data.length;
        });
    };
    $scope.init();

})
app.controller('AuthorDialogController', function ($scope, $modalInstance, authorService,$http) {
    $scope.deleteAuthor = function (authorIdDetails) {
        var authorId = authorIdDetails._id;
        if (authorIdDetails.isDeletedAuthor == 1) {
            authorService.deleteAuthorByIdDb(authorId).then(
                function (resultdata) {
                    var pageNumber=authorService.getAuthorSelectedPageNo();
                    $scope.getAuthorForPagination(pageNumber);
                    $modalInstance.close();
                })
        }
        else {
            authorService.unDeleteAuthorByIdDb(authorId).then(
                function (resultdata) {
                    var pageNumber=authorService.getAuthorSelectedPageNo();
                    $scope.getAuthorForPagination(pageNumber);
                    $modalInstance.close();
                })
        }

    };

});

