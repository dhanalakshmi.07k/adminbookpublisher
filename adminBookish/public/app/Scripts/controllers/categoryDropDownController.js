/**
 * Created by zendynamix on 12-05-2016.
 */

app.controller("categoryDropDownController", function ($scope,dataService) {
    function populateCategoryDropDown(){
        var categoryNameDropDown=[];
        dataService.getCategoryDropDown().then(
            function (payload) {
                for(var k=0;k<payload.data.length;k++){
                    categoryNameDropDown.push(payload.data[k].subCategory.name);
                    if (k == payload.data.length-1) {
                        $scope.categoryDropDown = categoryNameDropDown;
                    }
                }

            })

    }
    function init(){
        populateCategoryDropDown();
    }
    $scope.$on('categoryAdded', function (event, args) {
        populateCategoryDropDown();
    });


    init();

});
