/**
 * Created by zendynamix on 03-11-2015.
 */

var app = angular.module('myApp', ['ngRoute', 'angular.chosen', 'datatables',
    'blueimp.fileupload','ngAnimate', 'ui.bootstrap','colorpicker.module', 'wysiwyg.module','ng-sortable']);

app.controller('LoginController', function ($scope,$http,$window,$location) {
  $scope.registerSuccess=true;
  $scope.isLoading = false;
  $scope.login = function (user) {
    $scope.isLoading = true;
    $http
      .post('/login', user)
      .success(function (data, status, headers, config) {
        $window.sessionStorage.token = data.token;
        $location.path('/getAllBooksToCart');
          $scope.isLoading = false;
      })
      .error(function (data, status, headers, config) {
        // Erase the token if the user fails to log in
        delete $window.sessionStorage.token;
        $scope.isAuthenticated = false;
        // Handle login errors here
        $scope.error = 'Username or password is incorrect';
        $scope.welcome = '';
      });
  };

  $scope.logout = function () {
    delete $window.sessionStorage.token;
  };

  $scope.signUp = function (user) {
    $http
      .post('/signup', user)
      .success(function (data, status, headers, config) {
        $scope.successMessage="Successfully registered Please login";
        $location.path('/');
        $scope.registerSuccess=true;
      })
      .error(function (data, status, headers, config) {
        $scope.error =data ;
      });
  };




});
