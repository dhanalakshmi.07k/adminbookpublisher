/**
 * Created by zendynamix on 07-05-2016.
 */
app.controller("authorDropDownController", function ($scope,authorService) {
    function init(){
        getAuthoursForDropDown();
    }
    $scope.$on('authorAdded', function (event, args) {
        getAuthoursForDropDown();
    });
 function getAuthoursForDropDown(){
     console.log($scope.message);
     var nickNameArray=[];
     authorService.getAuthorsForDropDown().then(
         function (payload) {
             for(var i=0;i<payload.data.length;i++){
                 nickNameArray.push(payload.data[i].nickName);
             }
             $scope.authorNameForDropdown=nickNameArray;
         });

 }

    init();

});

