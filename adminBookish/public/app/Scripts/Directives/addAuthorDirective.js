/**
 * Created by zendynamix on 06-05-2016.
 */

app.controller("modalController", function($scope,authorService,$http,$modal) {
    $scope.openAddAuthor=function(author){
        $scope.message=null;
        if(author){
            $scope.oldNickName=author.nickName;
        }
      /*  $scope.authorData = angular.copy(author);*/
        $scope.modalInstance=$modal.open({
            templateUrl: '../../view/Modal/AddAuthor.html',
            scope:$scope,
            size: 'lg',
            animation: true,
        });
    }
    $scope.close=function(){
        $scope.modalInstance.dismiss();
    };
    $scope.add = function (author,oldNickName) {
        findUniqueAuthorByNickName(author)
    };
    function findUniqueAuthorByNickName(author) {
        var nickName = author.nickName;
        authorService.findAuthorByNickName(nickName).then(
            function (payload) {
                $scope.details = payload.data[0];
                if (!$scope.details) {
                    addAuthorToDb(author);
                }
                else {
                    $scope.message = "please provide Unique Nickname";
                }
            })
    }
    function addAuthorToDb(author) {
        authorService.getNumberOfAuthors().then(function (response) {
            var authorCount=response.data.NoOfAuthors;
            author.authorId = authorCount + 1;
            $http.post('/api/author', author
            ).success(function () {
                    $scope.close();
                    $scope.$emit('authorAdded', { currentPage: 1 });
                }).error(function (data) {
                    console.log("Error");
                });
        });
    }
    $scope.editAuthorDetails=function(author,oldNickName){
        $http.post('/api/author', author
        ).success(function () {
                authorService.pushAuthorSnapshot(oldNickName,author.nickName).then(
                    function (resultdata) {
                        authorService.pullAuthorSnapshot(oldNickName).then(
                            function (resultdata) {
                                var pageNumber=authorService.getAuthorSelectedPageNo();
                                $scope.close();
                                $scope.$emit('authorAdded', { currentPage: pageNumber });
                            })
                    })

            }).error(function (data) {
                console.log("Error");
            });
    }




})


app.directive("addAuthor", function() {
    return {
        restrict: 'E',
        controller:"modalController",
        scope:{
            authorData: '='
        },
        templateUrl: '../../view/templates/AddAuthor.html'


    }
});
