/**
 * Created by zendynamix on 12-05-2016.
 */
app.controller("categoryModelController", function($scope,$http,$modal,dataService) {
    $scope.openAddCategory = function (categoryDatanew) {
        $scope.statusMessage=null;
        /*$scope.categoryDatanew = angular.copy(categoryDatanew);*/
        $scope.getAllMainCategories();
       /* $scope.$emit('populateCategoryLabelDropDown', { message:'hello' });*/
        if(categoryDatanew){
            $scope.categoryDatanew = categoryDatanew;
            $scope.oldCategory = categoryDatanew.categoryName;
            $scope.oldSubCategory=categoryDatanew.subCategory.name
        }
        $scope.modalInstance = $modal.open({
            scope: $scope,
            templateUrl: './app/view/Modal/Category.html',
            size: 'lg',
            animation: true,
        });
    }
    $scope.close=function(){
        $scope.modalInstance.dismiss();
    };
    $scope.getAllMainCategories= function(){
        dataService.getAllMainName().then(function(response){
            $scope.mainCategoryList= response.data;
        });
    }
    function encodingFunction(CategoryLabelName) {
        var res = encodeURIComponent(CategoryLabelName);
        return res;
    }
    $scope.addSubCategory=function(category){
            var encodedNewSubCategory= encodingFunction(category.subCategory.name);
            var encodedcategory= encodingFunction(category.categoryName);
            dataService.findUniqueSubCategories(encodedcategory,encodedNewSubCategory).then(function(response){
                var status=response.data[0]
                if(!status){
                    $http.put('/api/SubCategory', category
                    ).success(function () {
                            $scope.close();
                            $scope.$emit('categoryAdded', { currentPage: 1 });
                            $scope.$emit('categoryDropDownController', { currentPage: 1 });
                        }).error(function () {
                            console.log("Error");
                        });
                }
                else{

                    $scope.statusMessage="category already exists"

                }

            });


    }
    $scope.updateSubCategoryDetails=function(category,oldCategory,oldSubCategory){
        //category label changed for category
        if(category.categoryName!=oldCategory){
            UpdateCategoryOnLabelChange(category,oldCategory,oldSubCategory);
        }else{
            //category label not changed name and decription changed
            updateCategoryDetails(category,oldCategory,oldSubCategory);
        }


    }
    function UpdateCategoryOnLabelChange(category,oldCategory,oldSubCategory){
        var encodedoldCategory= encodingFunction(oldCategory);
        var encodedoldSubCategory= encodingFunction(oldSubCategory);
        dataService.popSubcategory(encodedoldCategory,encodedoldSubCategory).then(
            function (payload) {
                $scope.updateSubCategory(category,encodedoldCategory,encodedoldSubCategory);
            })
    };
    function updateCategoryDetails(category,oldCategory,oldSubCategory){
        var encodedData= encodingFunction(category.categoryName);
        var encodedoldSubCategory= encodingFunction(oldSubCategory);
        var encodedNewSubCategory= encodingFunction(category.subCategory.name);
        var categoryObj={};
        categoryObj.subCategoryName=category.subCategory.name;
        categoryObj.subCategoryDescription=category.subCategory.description;
        categoryObj.category=oldCategory;
        categoryObj.oldSubCategoryName=oldSubCategory;
        $http.post('/update/subCategory',categoryObj
        ).success(function () {
                dataService.PushNewSubCategorySanpShot(encodedoldSubCategory,encodedNewSubCategory).then(
                    function (response) {
                        dataService.pullNewSubCategorySanpShot(encodedoldSubCategory).then(
                            function (response) {
                                var pageNumber=dataService.getPaginationPage();
                                $scope.$emit('categoryAdded', { currentPage:pageNumber });
                                if( $scope.searchPaginationCategory===1){
                                    var currentSearchPage =dataService.getSearchSelectedPageNo();
                                    var categorySearchParameter=dataService.getsearchCategoryName();
                                    $scope.categorySearchPagination(currentSearchPage,categorySearchParameter);
                                }
                                $scope.close();
                            })
                    })
            }).error(function () {
                console.log("Error");
            });
    }

    $scope.updateSubCategory=function(category,encodedoldCategory,encodedoldSubCategory){
        if (category&&category.categoryName && category.subCategory.name) {
            var encodedNewSubCategory= encodingFunction(category.subCategory.name);
            var encodedcategory= encodingFunction(category.categoryName)
                    $http.put('/api/SubCategory', category
                    ).success(function () {
                            if(encodedoldSubCategory){
                                dataService.PushNewSubCategorySanpShot(encodedoldSubCategory,encodedNewSubCategory).then(
                                    function (response) {
                                        dataService.pullNewSubCategorySanpShot(encodedoldSubCategory).then(
                                            function (response) {
                                                var pageNumber=dataService.getPaginationPage();
                                                $scope.$emit('categoryAdded', { currentPage:pageNumber });
                                                if( $scope.searchPaginationCategory===1){
                                                    var currentSearchPage =dataService.getSearchSelectedPageNo();
                                                    var categorySearchParameter=dataService.getsearchCategoryName();
                                                    $scope.categorySearchPagination(currentSearchPage,categorySearchParameter);
                                                }
                                                $scope.close();
                                            })
                                    })
                            }


                        }).error(function () {
                            console.log("Error");
                        });




        }
    }


})
app.directive("addCategory", function() {
    return {
        restrict: 'E',
        controller:"categoryModelController",
        scope:{
            mainCategoryData:"="
        },

        templateUrl: '../../view/templates/AddCategory.html'


    }
});