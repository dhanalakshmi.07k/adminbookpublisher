/**
 * Created by zendynamix on 07-04-2016.
 */

app.factory("authorService", function ($http) {
        var selectedPage;
        var totalAuthorCount=0;
        var authorNameForDropdown=null;
        var authorService = {
            getNumberOfAuthors:function(){
                return $http.get("/api/author/count");
            },

            getAuthorsForPagination:function(startLimit,endLimit){
                return $http.get("/api/author/"+startLimit+"/"+endLimit);

            },
            setAuthorPageNo:function(pageNo){
                selectedPage=pageNo;
            },
            getAuthorSelectedPageNo:function(){
                return selectedPage
            },
            setTotalAuthours:function(noOfAuthors){
                totalAuthorCount=noOfAuthors;
            },
            getTotalAuthours:function(){
                return totalAuthorCount;
            },
            findAuthorByNickName:function(nickName){
                return $http.get("/api/author/"+nickName);
            },
            pushAuthorSnapshot:function(oldNickName,newNickName){
                return $http.get("/push/authour/snapShot/Book/"+oldNickName+"/"+newNickName);
            },
            pullAuthorSnapshot:function(oldNickName,newNickName){
             return $http.get("/pull/authour/snapShot/Book/"+oldNickName);
            },
            deleteAuthorByIdDb:function(authorId){
                return $http.post("/api/archive/author/"+authorId);
            },
            unDeleteAuthorByIdDb:function(authorId){
                return $http.post("/api/unArchive/author/"+authorId);
            },
            getAuthorsForDropDown:function(){
                return $http.get("/author/nickNames");
            },
            getAuthorNameForDropdown : function() {
                return authorNameForDropdown;
            }

        }

    var getAuthoursNickNameForDropDown = function () {
        var nickNameArray=[];
        authorService.getAuthorsForDropDown().then(
            function (payload) {
                for(var i=0;i<payload.data.length;i++){
                    nickNameArray.push(payload.data[i].nickName);
                }
                authorNameForDropdown=nickNameArray;
            });

        }
    getAuthoursNickNameForDropDown();

        return authorService;




})