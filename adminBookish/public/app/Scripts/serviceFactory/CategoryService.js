/**
 * Created by zendynamix on 10-04-2016.
 */
app.factory('dataService', function($http) {
    var totalCategoryCount=0;
    var currentPage=0;
    var paginationNewPage=0;
    var CategoryPagination=0;

    return {
        getMainCategoriesForPagination:function(startimit,endLimit){
            return $http.get("/category/pagination/"+startimit+"/"+endLimit);
        },
        getMainCategoryCount:function(){
            return $http.get("/category/count");
        },
        setTotalCategoryLabel:function(noOfCategoryLabel){
            totalCategoryCount=noOfCategoryLabel;
        },
        getTotalCategoryLabel:function(){
            return totalCategoryCount;
        },

        setCurrentCategoryLabel:function(currentLabelPage){
            currentPage=currentLabelPage;
        },
        getCurrentCategoryLabel:function(){
            return currentPage;
        },

        findCategoryByName:function(categoryName){
            return $http.get("/api/CategoryName/"+categoryName);
        },
        CategoryByIdDb : function(id) {
            return $http.get("/api/CategoryUpdate/"+id );
        },
        deleteCategoryByIdDb : function(id) {
            return $http.get("/api/delete/Category/"+id );
        },
        storeSubCategorySortId:function(masterobj){
            return $http.post("/subCategory/update/bySortId/",masterobj);
        },

        getSubCategories: function(categoryName) {
            return $http.get("/api/sort/SubCategories/"+categoryName);
        },



        /*categories*/

        setPaginationPage:function(pageNo){
            paginationNewPage=pageNo;

        },
        getPaginationPage:function(){
            return  paginationNewPage;
        },
        getSubCategoriesForPagination:function(page,pageSize) {
            return $http.get("/Categories/subCategory/" + page + "/" + pageSize);
        },
        getSubCategoriesPaginationCount:function() {
            return $http.get("/subCategory/count");
        },
        getAllMainName:function(){
            return $http.get("/main/Categories");
        },
        PushNewSubCategorySanpShot:function(oldCategoryName,newCategoryName){
            return $http.get("/push/subCategory/snapShot/Book/"+oldCategoryName+"/"+newCategoryName);
        },
        pullNewSubCategorySanpShot:function(oldCategoryName){
            return $http.get("/pull/subCategory/snapShot/Book/"+oldCategoryName);
        },
        findUniqueSubCategories:function(CategoryName,subCategoryName) {
            return $http.get("/unique/Categories/subCategory/" + CategoryName + "/" + subCategoryName);
        },
        popSubcategory:function(oldCategory,oldSubCategory){
            return $http.get("/update/Categories/subCategory/name/"+oldCategory+"/"+oldSubCategory);

        },
        deleteSubcategory:function(categoryName,subCategoryName){
            return $http.get("/delete/subCategory/name/"+categoryName+"/"+subCategoryName);

        },
        deleteSubcategorySanpShot:function(subCategoryName){
            return $http.get("/delete/subCategory/snapShot/Book/"+subCategoryName);

        },
        getsearchCategoryName:function(){
            return searchCategoryName
        },
        setsearchCategoryName:function(searchCategoryNameParameter){
            searchCategoryName=searchCategoryNameParameter;
        },

        getSearchSelectedPageNo:function(){
            return selectedSearchPage
        },
        setSearchSelectedPageNo:function(pageNo){
            selectedSearchPage=pageNo;
        },
        getSearchSubCategoriesForPagination:function(searchCategoryName,page,pageSize) {
            return $http.get("/search/pagination/categoryLabel/categoryName/"+searchCategoryName+ "/" + page + "/" + pageSize);
        },
        getCategoryDropDown:function(){
            return $http.get("/subCategory/dropDown");
        },
        setCategoryPagination:function(pageNo){
            CategoryPagination=pageNo;
        },
        getCategoryPagination:function(){
            return CategoryPagination
        }

    }



    });
